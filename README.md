## Files
[./data/fr/homepage.yml](./data/fr/homepage.yml)

## Feature
- Landing page template
- Fully responsive
- Full screen navigation
- Contact form
- Portfolio filter
- Animated sections
- Foresty CMS settings included
- Multilingual

## Hugo based static files generation
https://gohugo.io/getting-started/configuration/

Forked from [Somrat repo](https://github.com/Somrat37/somrat.git)